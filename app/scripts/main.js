$(document).ready(function () {

  $('.nl-header__droper').click(function () {
    $('.nl-nav').slideToggle('fast', function () {
      // Animation complete.
    });
  });

  $('.nl-reviews__cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    autoplay: true,
    speed: 300,
    slidesToShow: 1
  });

  $('#nl-ad-1').hover(function () {
    $('.nl-dot_1').toggle();
  });

  $('#nl-ad-2').hover(function () {
    $('.nl-dot_2').toggle();
  });

  $('#nl-ad-3').hover(function () {
    $('.nl-dot_3').toggle();
  });

  $('#nl-ad-4').hover(function () {
    $('.nl-dot_4').toggle();
  });

  $('#nl-ad-5').hover(function () {
    $('.nl-dot_5').toggle();
  });

  $('#nl-ad-6').hover(function () {
    $('.nl-dot_6').toggle();
  });

});
